from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from scrappagofacilmodels.models import Office
from django.conf import settings


class BaseFunctionalTest(StaticLiveServerTestCase):

    def setUp(self):

        self.browser = webdriver.Chrome(settings.CHROME_DRIVER)
        self.browser.implicitly_wait(3)
        self.server_url = self.live_server_url

    def tearDown(self):
        self.browser.quit()

    def wait_for_element_with_id(self, element_id):
        WebDriverWait(self.browser, timeout=5).until(
            lambda b: b.find_element_by_id(element_id),
            'Could not find element with id {}. Page text was:\n{}'.format(
                element_id, self.browser.find_element_by_tag_name('body').text.encode('utf-8')
            )
        )

    def wait_for_element_by_xpath(self, xpath):
        WebDriverWait(self.browser, timeout=10).until(
            lambda b: b.find_element_by_xpath(xpath),
            'Could not find element with id {}. Page text was:\n{}'.format(
                xpath, self.browser.find_element_by_tag_name('body').text.encode('utf-8')
            )
        )

    def wait_for_url_contains(self, text):
        WebDriverWait(self.browser, timeout=30).until(
            lambda b:  text in b.current_url,
            'Url does not contain {}. Was:\n{}'.format(
                text, self.browser.current_url
            )
        )

    def create_test_data(self):
        # city = models.CharField(max_length=100)
        # province = models.CharField(max_length=100)
        # address = models.CharField(max_length=100)
        # name = models.CharField(max_length=100)
        # scrapped = JSONField()

        Office.objects.create(
            city='CABA', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped='')
        Office.objects.create(
            city='CABA', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped='')
        Office.objects.create(
            city='Buenos Aires', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped='')
        Office.objects.create(
            city='Buenos Aires', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped='')
        Office.objects.create(city='Cordoba', province='Cordoba', address='Avellaneda 2040', name='of 1', scrapped='')
