from .base import BaseFunctionalTest
from scrappagofacilmodels.models import Office


class OfficeDetailTest(BaseFunctionalTest):

    def test_displays_details(self):
        # I go to the details page of the first office
        self.create_test_data()
        office = Office.objects.first()
        self.browser.get('{}/map/{}'.format(self.server_url, office.id))

        # Check I am in the correct page
        self.assertIn("Oficinas RapiPago", self.browser.title)

        # I see all details of the office city='CABA', province='Buenos Aires',
        # address='Avellaneda 2040', name='of 1', scrapped='')
        province = self.browser.find_element_by_id('province').text
        self.assertEqual(office.province, province)

        city = self.browser.find_element_by_id('city').text
        self.assertEqual(office.city, city)

        address = self.browser.find_element_by_id('address').text
        self.assertEqual(office.address, address)

        name = self.browser.find_element_by_id('name').text
        self.assertEqual(office.name, name)

        # I can see a google maps displayed
        map_div = self.browser.find_element_by_xpath('//div[@name="map_canvas"]')

        # the map contains a marker for the office
        map_div = self.wait_for_element_by_xpath('//area[@title="my office"]')


        # there is a back button I can click to go back to the list
        # click back button and get to list with only this office displayed
        # the search box is filled in with the address of the queried office
