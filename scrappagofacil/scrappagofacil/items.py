# -*- coding: utf-8 -*-
from scrapy.contrib.djangoitem import DjangoItem
from scrappagofacilmodels.models import Office


class SucItem(DjangoItem):
    django_model = Office
# import scrapy


# class SucItem(scrapy.Item):
#     city = scrapy.Field()
#     province = scrapy.Field()
#     address = scrapy.Field()
#     name = scrapy.Field()
#     scrapped = scrapy.Field()
