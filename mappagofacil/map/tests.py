from django.test import TestCase
from django.core.urlresolvers import reverse
from scrappagofacilmodels.models import Office


class BaseMapTestCase(TestCase):

    def setUp(self):
        self.office_list = []
        self.create_test_data()

    def create_test_data(self):
        self.office_list.append(Office.objects.create(
            city='Buenos Aires', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped=''))
        self.office_list.append(Office.objects.create(
            city='Buenos Aires', province='Buenos Aires', address='Avellaneda 2042', name='of 1', scrapped=''))
        self.office_list.append(Office.objects.create(
            city='CABA', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped=''))
        self.office_list.append(Office.objects.create(
            city='CABA', province='Buenos Aires', address='Avellaneda 2041', name='of 1', scrapped=''))
        self.office_list.append(Office.objects.create(
            city='Cordoba', province='Cordoba', address='Avellaneda 2040', name='of 1', scrapped=''))


class OfficeMapTest(BaseMapTestCase):

    def test_template_used(self):
        response = self.client.get(reverse('office_map', kwargs={'pk': self.office_list[0].id}))
        self.assertTemplateUsed(response, 'map/office_detail.html')


class HomeTest(TestCase):

    def test_template_used(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'map/home.html')


class ListViewTest(BaseMapTestCase):

    def test_list_uses_template(self):
        response = self.client.get('/list')
        self.assertTemplateUsed(response, 'map/list.html')

    def test_data_in_context(self):
        response = self.client.get('/list')
        self.assertEqual(list(response.context['office_list']), self.office_list)

    def test_data_is_displayed(self):
        response = self.client.get('/list')
        self.assertContains(response, 'CABA')
        self.assertContains(response, 'Buenos Aires')
        self.assertContains(response, 'Cordoba')
        self.assertContains(response, 'Avellaneda 2040')
        self.assertContains(response, 'Avellaneda 2041')
        self.assertContains(response, 'Avellaneda 2042')
        self.assertContains(response, 'View In Map')

    def test_search_by_city_match(self):
        response = self.client.get('{}?search_by={}'.format(reverse('list'), 'CABA'))
        self.assertContains(response, 'CABA')
        self.assertEqual(2, len(response.context['office_list']))
        self.assertNotContains(response, 'Cordoba')

    def test_search_by_province_match(self):
        response = self.client.get('{}?search_by={}'.format(reverse('list'), 'Buenos'))
        self.assertContains(response, 'Buenos Aires')
        self.assertEqual(4, len(response.context['office_list']))
        self.assertNotContains(response, 'Cordoba')

    def test_search_by_address_match(self):
        response = self.client.get('{}?search_by={}'.format(reverse('list'), 'Avellaneda'))
        self.assertContains(response, 'Buenos Aires')
        self.assertEqual(5, len(response.context['office_list']))
        self.assertContains(response, 'Cordoba')

    def test_pagination_correct_in_content(self):
        for i in range(1, 5):
            self.create_test_data()

        response = self.client.get('/list')
        self.assertTrue(response.context['is_paginated'])
        self.assertEqual(response.context['page_obj'].paginator.num_pages, 3)
        self.assertEqual(10, len(response.context['office_list']))

    def test_pagination_links_present(self):
        for i in range(1, 5):
            self.create_test_data()

        response = self.client.get('/list')
        self.assertContains(response, "?page=2")
        self.assertContains(response, "?page=3")
        self.assertNotContains(response, "?page=4")
        self.assertContains(response, "next")
        self.assertContains(response, "previous")

    def test_pagination_links_not_present_if_only_one_page(self):
        response = self.client.get('/list')
        self.assertNotContains(response, "?page=2")
        self.assertContains(response, "next")
        self.assertContains(response, "previous")

    def test_pagination_links_present_in_page_other_than_1(self):
        for i in range(1, 5):
            self.create_test_data()

        response = self.client.get('/list?page=2')
        self.assertContains(response, "?page=2")
        self.assertContains(response, "?page=3")
        self.assertNotContains(response, "?page=4")
        self.assertContains(response, "next")
        self.assertContains(response, "previous")

    def test_pagination_links_present_in_last_page(self):
        for i in range(1, 5):
            self.create_test_data()

        response = self.client.get('/list?page=3')
        self.assertContains(response, "?page=2")
        self.assertContains(response, "?page=3")
        self.assertNotContains(response, "?page=4")
        self.assertContains(response, "next")
        self.assertContains(response, "previous")

    def test_pagination_displays_correct_results(self):
        for i in range(1, 5):
            self.create_test_data()

        response = self.client.get('/list?page=2')
        self.assertTrue(response.context['is_paginated'])
        self.assertEqual(response.context['page_obj'].paginator.num_pages, 3)
        self.assertEqual(list(response.context['office_list']), list(Office.objects.all())[10:20])
