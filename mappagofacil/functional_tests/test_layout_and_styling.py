from .base import BaseFunctionalTest


class LayoutAndStylingTest(BaseFunctionalTest):

    def test_layout_and_styling_home(self):
        # Edith goes to the home page
        self.browser.set_window_size(1024, 768)
        self.browser.get(self.server_url)

        # She notices the offices list is nicely centerd
        inputbox = self.browser.find_element_by_name('search_by')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            475,
            delta=5)

    def test_layout_and_styling_list(self):
        # Edith goes to the home page
        self.create_test_data()
        self.create_test_data()
        self.create_test_data()
        self.browser.get(self.server_url + '/list')
        self.browser.set_window_size(1024, 768)

        # She notices the offices list is nicely centerd
        centered_element = self.browser.find_element_by_name('province_1')
        self.assertAlmostEqual(
            centered_element.location['x'],
            186,
            delta=5
        )

        # She goes to a second page sees the input is nicely
        # centered there too
        next_link = self.browser.find_element_by_name('next')
        next_link.click()
        centered_element = self.browser.find_element_by_name('province_1')

        self.assertAlmostEqual(
            centered_element.location['x'],
            186,
            delta=5)
