from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.db.models import Q
from scrappagofacilmodels.models import Office
import logging


logger = logging.getLogger(__name__)


class OfficeListView(ListView):
    model = Office
    paginate_by = 10
    template_name = 'map/list.html'

    def get_queryset(self):
        queryset = super(OfficeListView, self).get_queryset()
        q = self.request.GET.get("search_by")
        if q:
            return queryset.filter(Q(city__icontains=q) |
                                   Q(province__icontains=q) |
                                   Q(address__icontains=q))
        return queryset


def homeView(request):
    return render(request, 'map/home.html')


class OfficeMap(DetailView):
    model = Office
    template_name = 'map/office_detail.html'
