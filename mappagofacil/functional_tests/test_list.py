from .base import BaseFunctionalTest


class OfficeListTest(BaseFunctionalTest):

    def test_displays_list_page(self):
        # go to home page
        self.create_test_data()
        self.browser.get(self.server_url + '/list')

        # Check I am in the correct page
        self.assertIn("Oficinas RapiPago", self.browser.title)
        self.assertEqual("Oficinas RapiPago", self.browser.find_element_by_tag_name("h2").text)
        search_input = self.browser.find_element_by_id("search")

        # I see a list of RapiPago offices, each with name and location
        # there is a group for each province and each city

        provinces = self.browser.find_elements_by_class_name("province")
        self.assertEqual(2, len(provinces))

        cities = self.browser.find_elements_by_class_name("city")
        self.assertEqual(3, len(cities))

        province_1 = self.browser.find_element_by_id('province_1')
        self.assertEqual('Buenos Aires', province_1.find_element_by_tag_name('span').text)
        ba_cities = province_1.find_elements_by_class_name('city')
        self.assertEqual(2, len(ba_cities))

        province_2 = self.browser.find_element_by_id('province_2')
        self.assertEqual('Cordoba', province_2.find_element_by_tag_name('span').text)
        cba_cities = province_2.find_elements_by_class_name('city')
        self.assertEqual(1, len(cba_cities))

        addresses = self.browser.find_elements_by_class_name('address')
        self.assertEqual(5, len(addresses))

        map_links = self.browser.find_elements_by_link_text('View In Map')
        self.assertEqual(5, len(map_links))

        map_links[0].click()
        self.wait_for_url_contains('/map')
        address = self.browser.find_element_by_class_name('address')


    def test_list_pagination(self):
        # create many items so that we have the need to paginate
        for i in range(1, 6):
            self.create_test_data()

        # go to home page
        self.browser.get(self.server_url + '/list')

        # As the list is big, it is paginated
        next_link = self.browser.find_element_by_name("next")
        self.assertIn('page=2', next_link.get_attribute('href'))
        page2_link = self.browser.find_element_by_name("page_2")

        # I go to the next page and check the url indicates page number
        page2_link.click()
        self.assertIn('page=2', self.browser.current_url)

        # It also starts and ends with the correct element
        cities = self.browser.find_elements_by_class_name("city")
        self.assertEqual('CABA', cities[0].text)
        self.assertEqual('CABA', cities[-1].text)
