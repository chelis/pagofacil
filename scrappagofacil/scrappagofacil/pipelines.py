# -*- coding: utf-8 -*-
import re
import json


class ScrappagofacilPipeline(object):

    def process_item(self, item, spider):
        item['scrapped'] = self.build_json(item)
        item['address'] = self.cleanup_address(item['address'])

        item.save()
        return item

    def cleanup_address(self, address):
        m = re.search('(?P<numb>(\d+))\s(?P=numb)', address)
        if m:
            return address[0:m.end(1)]
        return address

    def build_json(self, item):
        return json.dumps({'address': item['address'],
                           'province': item['province'],
                           'city': item['city'],
                           'name': item['name']})
