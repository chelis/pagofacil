# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('scrappagofacilmodels', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='office',
            name='scrapped',
            field=jsonfield.fields.JSONField(default=''),
            preserve_default=False,
        ),
    ]
