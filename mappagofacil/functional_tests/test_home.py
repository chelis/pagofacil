from .base import BaseFunctionalTest
from selenium.common.exceptions import NoSuchElementException


class HomeTest(BaseFunctionalTest):

    def test_displays_home_page(self):
        # go to home page
        self.create_test_data()
        self.browser.get(self.server_url)

        # Check I am in the correct page
        self.assertIn("Oficinas RapiPago", self.browser.title)
        self.assertEqual("Oficinas RapiPago", self.browser.find_element_by_tag_name("h2").text)
        search_input = self.browser.find_element_by_id("search_by")
        self.assertEqual(0, len(self.browser.find_elements_by_class_name('province')))

        # I dont see a list of RapiPago offices
        self.assertEqual(0, len(self.browser.find_elements_by_class_name('province')))
        self.assertRaises(NoSuchElementException, self.browser.find_element_by_name, 'page_1')

        search_btn = self.browser.find_element_by_id("btn-search")
        # fill in the input box and click on the search button
        search_input.send_keys('CABA')
        search_btn.click()

        # I get to the list page, and I see only CABA results.
        self.wait_for_url_contains('/list')
        self.assertEqual(1, len(self.browser.find_elements_by_class_name('city')))
