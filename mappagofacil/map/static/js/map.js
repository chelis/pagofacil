var initialize = function (latlong, zoom, title, div_id, optimized) {
     latlng = new google.maps.LatLng(latlong[0], latlong[1])
     map =  new google.maps.Map($("#" + div_id).get(0),{center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP, zoom: zoom})
     window.office_map = map // put map in global scope for further reference
     marker = new google.maps.Marker({position:latlng, map:office_map, title:title, optimized: optimized})
    };

window.Map = {

    initialize: initialize 
}


