from django.test import TestCase
from .models import Office

class OfficeTest(TestCase):
        def test_str(self):
            office = Office.objects.create(
            city='CABA', province='Buenos Aires', address='Avellaneda 2040', name='of 1', scrapped='')

            self.assertEqual("Office[city='CABA', province='Buenos Aires', address='Avellaneda 2040', name='of 1']", office.__str__())



