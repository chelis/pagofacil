# -*- coding: utf-8 -*-

# Scrapy settings for scrappagofacil project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
import sys
import os

sys.path.append('/home/chelis/development/mine/mappagofacil/mappagofacil')
os.environ['DJANGO_SETTINGS_MODULE'] = 'mappagofacil.settings.settings'

BOT_NAME = 'scrappagofacil'

SPIDER_MODULES = ['scrappagofacil.spiders']
NEWSPIDER_MODULE = 'scrappagofacil.spiders'

ITEM_PIPELINES = {
    'scrappagofacil.pipelines.ScrappagofacilPipeline': 300,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'scrappagofacil (+http://www.yourdomain.com)'
