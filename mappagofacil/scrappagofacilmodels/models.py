from django.db import models
from jsonfield import JSONField


class Office(models.Model):
    city = models.CharField(max_length=100)
    province = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    scrapped = JSONField()

    def __str__(self):
        return "Office[city='{}', province='{}', address='{}', name='{}']".format(self.city, self.province, self.address, self.name)

    class Meta:
        ordering = ['province', 'city', 'address']
