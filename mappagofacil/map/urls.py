from django.conf.urls import patterns, url
from .views import OfficeListView, homeView, OfficeMap
urlpatterns = patterns('',
                       url(r'^$', homeView, name='home'),
                       url(r'^list$', OfficeListView.as_view(), name='list'),
                       url(r'^map/(?P<pk>\d+)$', OfficeMap.as_view(), name='office_map'),
                       )
