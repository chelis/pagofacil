from django import template
from django.conf import settings

register = template.Library()

class DebugNode(template.Node):
    def __init__(self, debug):
        self.debug = debug

    def render(self, context):
        return self.debug

@register.tag
def map_optimized(parser, token):
    debug = 'true' if getattr(settings, 'TEMPLATE_DEBUG') else 'false'
    return DebugNode(debug)


